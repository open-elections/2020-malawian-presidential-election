# 2020 Malawian presidential election

    start_date: 2020-06-23
    end_date: 2020-06-23
    source: https://fpe2020.com/
    wikipedia: https://en.wikipedia.org/wiki/2020_Malawian_presidential_election

**The candidates file does not specify which candidates should be active. If you want any candidates to show up on Open Elections, add a column with the header `active` and add `TRUE` to any candidate you want to include.**